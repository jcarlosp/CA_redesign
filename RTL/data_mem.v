
module data_mem(
		input wire			       clk,
		input wire	[DATA_W-1:0] addr,
		input wire	[DATA_W-1:0] addr_ext,
      input wire               wen,
      input wire               wen_ext,
		input wire 	[DATA_W-1:0] wdata,
		input wire 	[DATA_W-1:0] wdata_ext,
		output wire	[DATA_W-1:0] rdata
		output wire	[DATA_W-1:0] rdata_ext

   );
parameter integer ADDR_W      = 8;
parameter integer SEL_W       = ADDR_W-7;
parameter integer MACRO_DEPTH = 128;
parameter integer N_MEMS      = 2**(ADDR_W)/MACRO_DEPTH;


reg [      7:0] addr_i, addr_ext_i;
reg [SEL_W-1:0] mem_sel;
reg [     32:0] data_i [0:SEL_W-1];
reg [     32:0] data_ext_i [0:SEL_W-1];
reg             cs_i   [0:(2**SEL_W)-1];
 

always@(*)begin
   addr_i     = addr    [6:0];
   addr_ext_i = addr_ext[6:0];
end

genvar index_depth;
generate
   for (index_depth = 0; index_depth < N_MEMS; index_depth = index_depth+1) begin:
         always@(*) begin
            if(mem_sel == index_depth) begin
               cs_i[index_depth] == 1'b0;
            end else begin
               cs_i[index_depth] == 1'b1;
            end
         end
      
         SRAM2RW128X32 dram_inst (
            .A1(addr_),
            .A2(addr_ext),
            .CE1(clk),
            .CE2(clk),
            .WEB1(wen),
            .WEB2(wen_ext),
            .OEB1(cs_i[index_depth]),
            .OEB2(cs_i[index_depth]),
            .CSB1(cs_i[index_depth]),
            .CSB2(cs_i[index_depth]),
            .I1($unsigned(wdata)),
            .I2($unsigned(wdata_ext)),
            .O1(data_i[index_depth]),
            .O2(data_ext_i[index_depth])
         );
   end
endgenerate

assign rdata     = data_i    [mem_sel][DATA_W-1:0];
assign rdata_ext = data_ext_i[mem_sel][DATA_W-1:0]; 
   


	
endmodule

`endif

