module register_file#(
   parameter integer DATA_W     = 16
)(
      input  wire              clk,
      input  wire              arst_n,
      input  wire              reg_write,
      input  wire [       4:0] raddr_1,
      input  wire [       4:0] raddr_2,
      input  wire [       4:0] waddr,
      input  wire [DATA_W-1:0] wdata,
      output reg  [DATA_W-1:0] rdata_1,
      output reg  [DATA_W-1:0] rdata_2
   );

   parameter integer N_REG      = 32;

   reg [DATA_W-1:0] reg_array     [0:N_REG-1];
   reg [DATA_W-1:0] reg_array_nxt [0:N_REG-1];


   integer idx;


   always@(*) begin
      if((raddr_1 == waddr) && (reg_write == 1'b1)) begin
         rdata_1 = wdata;
      end else begin
         rdata_1 = reg_array[raddr_1];
      end
   end

   always@(*) begin
      if((raddr_2 == waddr) && (reg_write == 1'b1)) begin
         rdata_2 = wdata;
      end else begin
         rdata_2 = reg_array[raddr_2];
      end
   end

   //Register file write process
   always@(*) begin
      for(idx=0; idx<N_REG; idx =idx+1)begin
         if((reg_write == 1'b1) && (waddr == idx)) begin
            reg_array_nxt[idx] = wdata;
         end else begin
            reg_array_nxt[idx] = reg_array[idx];
         end
      end
   end

   always@(posedge clk, negedge arst_n) begin
      if(arst_n == 1'b0)begin
         for(idx=0; idx<N_REG; idx =idx+1)begin
            reg_array[idx] <= 'b0;
         end
      end else begin
         for(idx=0; idx<N_REG; idx =idx+1)begin
            reg_array[idx] <= reg_array_nxt[idx];
         end
      end
   end

endmodule



