module#(
   parameter integer DATA_IN_W  = 8,
   parameter integer DATA_W     = 16
   )sign_extend(
      input  signed wire [DATA_IN-1:0] val,
      output signed reg  [DATA_W-1:0]  sign_ext_val
   );

   
   always@(*) sign_ext_val =  val;

endmodule


