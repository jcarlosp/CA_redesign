module cpu_tb;


reg       	 clk;
reg          arst_n;
reg  [31:0]  addr_ext;
reg          wen_ext;
reg          ren_ext;
reg  [31:0]  wdata_ext;
reg  [31:0]  addr_ext_2;
reg          wen_ext_2;
reg          ren_ext_2;
reg  [31:0]  wdata_ext_2;
wire [31:0]  rdata_ext;
wire [31:0]  rdata_ext_2;
reg         enable;

integer half_clock_period_ns = 50;
integer imem_cnt, dmem_cnt;
parameter integer IMEM_SIZE = 2**9;
parameter integer DMEM_SIZE = 2**10;

reg [31:0] instr_mem [0:IMEM_SIZE-1];
reg [31:0] data_mem [0:DMEM_SIZE-1];

initial begin
   clk    = 1'b0;
   arst_n = 1'b0;
   enable = 1'b0;
   addr_ext = 'b0;
   wen_ext  = 1'b0;
   ren_ext  = 1'b0;
   addr_ext = 'b0;
   wdata_ext = 'b0;
   wen_ext_2  = 1'b0;
   ren_ext_2  = 1'b0;
   addr_ext_2 = 'b0;
   wdata_ext_2 = 'b0;
   cnt_and_wait(10);
   arst_n = 1'b1;

   load_dmem();
   load_imem();
   
   cnt_and_wait(1);
   enable = 1'b1;
  cnt_and_wait(1000);

   $stop;


end


always #half_clock_period_ns begin
   clk = ~clk;
end

cpu dut(
   .clk         (clk        ),
   .enable      (enable     ),
   .arst_n      (arst_n     ),
   .addr_ext    (addr_ext   ),
   .wen_ext     (wen_ext    ),
   .ren_ext     (ren_ext    ),
   .wdata_ext   (wdata_ext  ),
   .addr_ext_2  (addr_ext_2 ),
   .wen_ext_2   (wen_ext_2  ),
   .ren_ext_2   (ren_ext_2  ),
   .wdata_ext_2 (wdata_ext_2),
   .rdata_ext   (rdata_ext  ),
   .rdata_ext_2 (rdata_ext_2)
);



task load_imem();
begin
   for (imem_cnt = 0; imem_cnt < IMEM_SIZE; imem_cnt = imem_cnt+1)  begin
      instr_mem[imem_cnt] = 'b0;
   end
    $readmemh("../RTL/imem_content.txt",instr_mem);
   cnt_and_wait(10);
   
   for (imem_cnt = 0; imem_cnt < IMEM_SIZE; imem_cnt = imem_cnt+1)  begin
      wait(clk==1'b0);
      wen_ext   = 1'b1;
      ren_ext   = 1'b0;
      wdata_ext = instr_mem[imem_cnt];
      addr_ext  = imem_cnt<<2;
      wait(clk==1'b1);
      
   end
   wen_ext   = 1'b0;
   ren_ext   = 1'b0;
   wdata_ext = 'b0;
   addr_ext  = 'b0;
end
endtask 


task load_dmem();
begin
   for (dmem_cnt = 0; dmem_cnt < DMEM_SIZE; dmem_cnt = dmem_cnt+1)  begin
      data_mem[dmem_cnt] = 'b0;
   end
    $readmemh("../RTL/dmem_content.txt",data_mem);
   cnt_and_wait(10);
   
   for (dmem_cnt = 0; dmem_cnt < DMEM_SIZE; dmem_cnt = dmem_cnt+1)  begin
      wait(clk==1'b0);
      wen_ext_2   = 1'b1;
      ren_ext_2   = 1'b0;
      wdata_ext_2 = data_mem[dmem_cnt];
      addr_ext_2  = dmem_cnt<<2;
      wait(clk==1'b1);
      
   end
   wen_ext_2   = 1'b0;
   ren_ext_2   = 1'b0;
   wdata_ext_2 = 'b0;
   addr_ext_2  = 'b0;
end
endtask 




task cnt_and_wait();
input [31:0] stop_counter;
integer cnt_cycles;
begin
   for(cnt_cycles = 0; cnt_cycles < stop_counter; cnt_cycles = cnt_cycles+1) begin
      wait(clk==1'b1);
      wait(clk==1'b0);
   end
end
endtask



endmodule
